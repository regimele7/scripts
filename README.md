# Scripts

Requirements
	PC/Laptop
	RAM - Minimum of 4GB
	CPU - Minimum of Intel CPU core i5 2-nd generation
	Python 3.11 (Python Idle/Python CMD)
	data.excel file

How to run
	Download the script and data files and make sure they are on the same folder
	Navigate to the script directory
	Create python local environment
	    python -m venv venv
	    .\venv\Scripts\activate
	Install requirements
	    pip install -r requirements.txt
	Run the script

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/regimele7/scripts.git
git branch -M main
git push -uf origin main
```
