import numpy as np
from sklearn.decomposition import PCA
from PIL import Image
import xlsxwriter
# from deepface import DeepFace
import uuid
import os


def get_image_arr(img):
    opened_img = Image.open(img.path)
    image_array = np.array(opened_img)
    image_flat = image_array.reshape(image_array.shape[0], -1)
    pca = PCA(n_components=20)
    image_pca = pca.fit_transform(image_flat)
    return str(image_pca.tolist())



# def get_dominant_emotion(img):
#     obj = DeepFace.analyze(img_path=img, actions=['emotion'])
#     return obj[0].get('dominant_emotion')
#
#
# def get_dominant_age(img):
#     obj = DeepFace.analyze(img_path=img, actions=['age'])
#     return obj[0].get('age')
#
#
# def get_dominant_gender(img):
#     obj = DeepFace.analyze(img_path=img, actions=['gender'])
#     return obj[0].get('dominant_gender')


if __name__ == '__main__':
    workbook = xlsxwriter.Workbook('hello.xlsx')
    worksheet = workbook.add_worksheet('First Worksheet')
    directory = 'faces94'

    for i in range(0, 21):
        worksheet.write(0, i, f'Image {i}')
    worksheet.write(f'A1', 'Person UUID')

    row = 1
    for outer_file in os.scandir(directory):
        for inner_file in os.scandir(outer_file):
            worksheet.write(row, 0, str(uuid.uuid4()))
            col = 1
            for image in os.scandir(inner_file):
                # if row == 1 and col == 1:
                #     worksheet.write(row, col, get_dominant_emotion(image.path))
                worksheet.write(row, col, get_image_arr(image))
                col += 1
            row += 1

    workbook.close()
