import pandas as pd
import random
from timeit import default_timer as timer

if __name__ == '__main__':
    start1 = timer()

    df = pd.read_excel('DataBirthday.xlsx')
    shuffled_df = df.copy()

    shuffled_names = shuffled_df['First and Last Name'].tolist()
    shuffled_df['index'] = range(len(shuffled_names))
    for index, name in enumerate(shuffled_names):
        try:
            shuffled_df['index'].append(index)
        except Exception:
            continue

    start = timer()

    temp = list(zip(shuffled_df['First and Last Name'].tolist(), shuffled_df['index']))
    random.shuffle(temp)
    names, indexes = zip(*temp)

    temp = list(zip(shuffled_df['Birthday'].tolist(), shuffled_df['Credit Card Number'].tolist()))
    random.shuffle(temp)
    birthday, ccn = zip(*temp)

    shuffled_df['First and Last Name'] = names
    shuffled_df['index'] = indexes
    shuffled_df['Birthday'] = birthday
    shuffled_df['Credit Card Number'] = ccn
    end = timer()
    print('Exe. time: ', end - start)

    # Write the shuffled DataFrame to a new Excel file
    with pd.ExcelWriter('shuffled_data.xlsx') as writer:
        shuffled_df.to_excel(writer, index=False)

    end1 = timer()
    print('Exe. time total: ', end1 - start1)
