import pandas as pd
import random
from timeit import default_timer as timer


# Fisher-Yates shuffle
def fisher_yates_shuffle(df):
    indices = list(df.index)
    random.shuffle(indices)
    return df.iloc[indices].reset_index(drop=False)


# Random sampling without replacement
def random_sampling_shuffle(df):
    return df.sample(frac=1).reset_index(drop=False)


if __name__ == '__main__':
    # Read the Excel file
    df = pd.read_excel('data.xlsx')

    # Perform shuffling using Fisher-Yates shuffle
    start = timer()
    shuffled_df_fisher_yates = fisher_yates_shuffle(df)
    end = timer()
    print('Fisher yates algorythm time: ', end - start)

    # Perform shuffling using random sampling without replacement
    start = timer()
    shuffled_df_random_sampling = random_sampling_shuffle(df)
    end = timer()
    print('Default algorythm time: ', end - start)

    # Write the shuffled DataFrames to a new Excel file
    with pd.ExcelWriter('shuffled_data.xlsx') as writer:
        shuffled_df_fisher_yates.to_excel(writer, sheet_name='Fisher-Yates Shuffle', index=False)
        shuffled_df_random_sampling.to_excel(writer, sheet_name='Random Sampling', index=False)
